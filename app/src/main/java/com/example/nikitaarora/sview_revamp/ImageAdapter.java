package com.example.nikitaarora.sview_revamp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by nikita.arora on 11/25/2015.
 */
public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {
    Picasso mPicasso;
    private Context mContext;
    public List<Brands> brandList;

    public ImageAdapter(Context context, List<Brands> itemList) {
        brandList = itemList;
        mContext = context;
        this.mPicasso = Picasso.with(context);
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder {
        TextView brandName;
        ImageView brandImg;
        CardView cardView;

        public ImageViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
            brandName = (TextView) itemView.findViewById(R.id.brand_txt);
            brandImg = (ImageView) itemView.findViewById(R.id.brand_img);
        }
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_layout, null);
        ImageViewHolder imgViewHolder = new ImageViewHolder(layoutView);
        return imgViewHolder;
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, final int position) {
        holder.brandName.setText(brandList.get(position).getBrandName());
        mPicasso.load(brandList.get(position).getBrandImg()).into(holder.brandImg);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CouponActivity.class);
                intent.putExtra("Position", position);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return brandList.size();
    }

}
