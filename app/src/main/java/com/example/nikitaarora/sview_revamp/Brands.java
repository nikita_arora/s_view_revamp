package com.example.nikitaarora.sview_revamp;

/**
 * Created by Nikita on 07-06-2016.
 */
public class Brands {
    private String brandName;
    private int brandImg;

    public Brands(String name, int image)  {
        this.brandName = name;
        this.brandImg = image;
    }

    public String getBrandName() {
        return brandName;
    }

    public int getBrandImg() {
        return brandImg;
    }

}
