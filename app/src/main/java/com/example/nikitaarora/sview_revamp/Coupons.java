package com.example.nikitaarora.sview_revamp;

/**
 * Created by Nikita on 07-06-2016.
 */
public class Coupons {
    private String brandName;
    private int couponImg;

    public Coupons(String name, int image)  {
        this.brandName = name;
        this.couponImg = image;
    }

    public String getBrandName() {
        return brandName;
    }

    public int getCouponImg() {
        return couponImg;
    }
}
