package com.example.nikitaarora.sview_revamp;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.github.fafaldo.fabtoolbar.widget.FABToolbarLayout;

public class CouponActivity extends AppCompatActivity{

    ViewPager viewPager;
    SwipeAdapter adapter;
    Intent intent;
    int position;

    private FABToolbarLayout fabLayout;
    private View email, sms, fab;

    String phone;

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.setTitle(getString(R.string.coupon_activity));
        viewPager = (ViewPager) findViewById(R.id.pager);
        fabLayout = (FABToolbarLayout) findViewById(R.id.fab_toolbar);
        email = findViewById(R.id.email);
        sms = findViewById(R.id.sms);
        fab = findViewById(R.id.fab_toolbar_fab);

        intent = getIntent();
        Bundle extras = intent.getExtras();

        if (extras != null) {
            position = extras.getInt("Position");
        }

        adapter = new SwipeAdapter(this, position);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(position);
        adapter.checkActivity(15000);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.checkActivity(15000);
                fabLayout.show();
            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterDetailsDialog(2);
            }
        });

        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterDetailsDialog(1);
            }
        });
    }

    private void enterDetailsDialog(final int selected) {
        adapter.checkActivity(60000);
        Log.d("selected", String.valueOf(selected));

        LayoutInflater inflate = LayoutInflater.from(CouponActivity.this);
        View promptView = inflate.inflate(R.layout.enter_number_dialog, null);
        final EditText text = (EditText) promptView.findViewById(R.id.enter_text);
        if (selected == 1)
        {
            text.setHint("Enter number..");
            text.setInputType(InputType.TYPE_CLASS_NUMBER);
        }

        else if (selected == 2)
        {
            text.setHint("Enter E-Mail id..");
            text.setInputType(InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS);
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CouponActivity.this);
        dialogBuilder.setView(promptView);

        dialogBuilder.setCancelable(false)
                .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (selected == 1)
                        {
                            phone = text.getText().toString();
                            checkPermission();
                        }

                        else if (selected == 2)
                        {
                            String email = text.getText().toString();
                            String message = "Sending coupon code..";
                            sendEmail(email, message);
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = dialogBuilder.create();
        alert.show();
    }

    private void sendEmail(String email, String message) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
        intent.putExtra(Intent.EXTRA_TEXT, message);
        intent.setData(Uri.parse("mailto:"+email));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermission()  {
        int hasSendSMSPermission = checkSelfPermission(Manifest.permission.SEND_SMS);
        if (hasSendSMSPermission != PackageManager.PERMISSION_GRANTED)  {
            requestPermissions(new String[] {Manifest.permission.SEND_SMS}, REQUEST_CODE_ASK_PERMISSIONS);
        }   else    {
            sendSMS(phone);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode)    {
            case REQUEST_CODE_ASK_PERMISSIONS:  {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)   {
                    sendSMS(phone);
                }   else    {
                    Toast.makeText(this, "SMS Permission denied", Toast.LENGTH_SHORT).show();
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    private void sendSMS(String phone) {
        checkPermission();
        String message = "Sending coupon code..";
        PendingIntent pi = PendingIntent.getActivity(this, 0, new Intent(this, CouponActivity.class), 0);
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phone, null, message, pi, null);
    }

    @Override
    public void onBackPressed() {
        if (fabLayout.isToolbar())  {
            Log.i("back pressed", "toolbar present");
            fabLayout.hide();
            adapter.checkActivity(15000);
        }
        else {
            Log.i("back pressed", "toolbar not present");
            super.onBackPressed();
        }
    }
}
