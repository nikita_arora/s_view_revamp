package com.example.nikitaarora.sview_revamp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    ImageAdapter imageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);

        List<Brands> brandsList = getListItems();
        imageAdapter = new ImageAdapter(this, brandsList);
        recyclerView.setAdapter(imageAdapter);
    }

    private List<Brands> getListItems() {
        List<Brands> listItems = new ArrayList<>();
        listItems.add(new Brands("Calvin Klein", R.drawable.ck));
        listItems.add(new Brands("Versace", R.drawable.img11));
        listItems.add(new Brands("Prada", R.drawable.prada_2));
        listItems.add(new Brands("Puma", R.drawable.img13));
        listItems.add(new Brands("Swarovski", R.drawable.swarovski_new));
        listItems.add(new Brands("Fossil", R.drawable.img16));
        listItems.add(new Brands("Tommy Hilfiger", R.drawable.img17));
        listItems.add(new Brands("Dolce & Gabbana", R.drawable.img18));
        listItems.add(new Brands("Gucci", R.drawable.img19));
        listItems.add(new Brands("Adidas", R.drawable.img2));
        listItems.add(new Brands("Louis Vuitton", R.drawable.img4));
        listItems.add(new Brands("Marks & Spencer", R.drawable.img5));
        listItems.add(new Brands("Burger King", R.drawable.img6));
        listItems.add(new Brands("Zara", R.drawable.img7));
        listItems.add(new Brands("Mango", R.drawable.img8));
        listItems.add(new Brands("Starbucks Coffee", R.drawable.img9));

        return listItems;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_about) {
            Intent i = new Intent(getApplicationContext(), AboutActivity.class);
            startActivity(i);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}