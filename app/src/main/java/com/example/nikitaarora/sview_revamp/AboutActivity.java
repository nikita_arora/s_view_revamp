package com.example.nikitaarora.sview_revamp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


public class AboutActivity extends AppCompatActivity {
    private Handler handle = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.setTitle(getString(R.string.about_activity_title));
    }

    @Override
    protected void onResume() {
        super.onResume();
        handle.postDelayed(openMain, 10000);
    }


    @Override
    protected void onStop() {
        super.onStop();
        handle.removeCallbacks(openMain);
    }

    private final Runnable openMain = new Runnable() {
        @Override
        public void run() {
            try {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    };
}
