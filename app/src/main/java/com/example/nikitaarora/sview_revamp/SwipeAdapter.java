package com.example.nikitaarora.sview_revamp;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikita on 07-06-2016.
 */
public class SwipeAdapter extends PagerAdapter {
    Picasso mPicasso;
    private Context mcontext;
    private LayoutInflater inflater;
    List<Coupons> couponsList;
    int currentPosition;
    private android.os.Handler handle = new Handler();

    public SwipeAdapter(Context ctx, int position)    {
        this.mcontext = ctx;
        this.currentPosition = position;
        couponsList = getCouponList();
        this.mPicasso = Picasso.with(ctx);
    }

    private List<Coupons> getCouponList() {
        couponsList = new ArrayList<>();
        couponsList.add(new Coupons("Calvin Klein", R.drawable.ck_cop));
        couponsList.add(new Coupons("Versace", R.drawable.versace));
        couponsList.add(new Coupons("Prada", R.drawable.prada));
        couponsList.add(new Coupons("Puma", R.drawable.puma));
        couponsList.add(new Coupons("Swarovski", R.drawable.swa));
        couponsList.add(new Coupons("Fossil", R.drawable.fossil));
        couponsList.add(new Coupons("Tommy Hilfiger", R.drawable.tommy));
        couponsList.add(new Coupons("Dolce & Gabbana", R.drawable.dg));
        couponsList.add(new Coupons("Gucci", R.drawable.gucci));
        couponsList.add(new Coupons("Adidas", R.drawable.adidas));
        couponsList.add(new Coupons("Louis Vuitton", R.drawable.l_v));
        couponsList.add(new Coupons("Marks & Spencer", R.drawable.m_s));
        couponsList.add(new Coupons("Burger King", R.drawable.b_k));
        couponsList.add(new Coupons("Zara", R.drawable.zara));
        couponsList.add(new Coupons("Mango", R.drawable.mango));
        couponsList.add(new Coupons("Starbucks Coffee", R.drawable.star));

        return couponsList;
    }

    @Override
    public int getCount() {
        return couponsList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.fragment_coupon_slide, container, false);
        ImageView imgView = (ImageView) itemView.findViewById(R.id.coupon_img);
        TextView txtView = (TextView) itemView.findViewById(R.id.coupon_txt);

        txtView.setText(couponsList.get(position).getBrandName());
        mPicasso.load(couponsList.get(position).getCouponImg()).into(imgView);

        LinearLayout coupon_linear = (LinearLayout) itemView.findViewById(R.id.coupon_linear);
        coupon_linear.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                    checkActivity(15000);
                    return true;
            }
        });

        container.addView(itemView);
        return itemView;
    }

    public void checkActivity(int timer) {
        Log.i("Adapter call", "onTouch triggerred..");
        handle.removeCallbacks(openMain);
        handle.postDelayed(openMain, timer);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    public final Runnable openMain = new Runnable() {
        @Override
        public void run() {
            try {
                Intent intent = new Intent(mcontext, MainActivity.class);
                mcontext.startActivity(intent);
            }   catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}
